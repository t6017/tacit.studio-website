---
title: "creative image lab"
draft: false
cover: /images/creative-image-lab-2.jpg

---

### Creative Image Lab is een werkplaats voor ideëen. Een plek om te onderzoeken, te ontwikkelen en te maken midden in het centrum van Hilversum.

Creative Image Lab is gestart tijdens de dutch media week van oktober 2019 in een samenwerking tussen Beeld en Geluid en Grafisch Atelier Hilversum. In 2020 werd het lab door GAH en tacit.studio doorontwikkeld tot creative hub voor het culturele veld van Hilversum.

## De lokatie wordt gebruikt o.a. ingezet voor:

- [Hilversum Mediawijs](https://hilversummediawijs.nl/over/) – co-creatie van lessen mediawijsheid
- [Het familieatelier](https://gahilversum.nl/familieatelier/) van Grafisch Atelier Hilversum
- Thuisbasis van de cultuurcoaches
- [De labsessies](/blog/lab-sessies/) van tacit.studio

## Adres

Kerkstraat 63-35  
1211CL Hilversum


