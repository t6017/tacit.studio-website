---
Title: home

# cover: /images/tacit-aanbod.png
---
# Creativiteit en verbeelding voor professional, team & project

## **Professional**: maak een begin met [Print & Play](/print-play/), en geef het inhoud in je [Creative Journal](/creative-journal/). Ben je leerkracht of docent? Ga [Hardop Doen](/hardop-doen/)

## **Team:** vind en verbeeld je collectieve ambitie met het [Visueel Manifest](/visueel-manifest/)

## **Project:** tacit.studio [initieert en faciliteert de creatieve ontwikkeling](/diensten/maatwerk-projecten/) van organisatie, wijk en stad.
