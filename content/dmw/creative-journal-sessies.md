---
title: "Workshop Creative Journal"
description: "Kickstart je makerschap tijdens de creative journal sessies in het Creative Image Lab."
draft: false
cover: /images/promo-creative-journal.jpg

---

Creativiteit, verbeelding, ‘design thinking’, we worden allemaal geacht het te kennen en toe te passen. Maar wat houdt het in om creatief te zijn? Waar komt een idee vandaan en hoe “vang” je het? En wat maak je er dan van?

### Hét onmisbare instrument voor elke creatieve maker en visionaire veranderaar is haar schets- en notitieboek

Achter elk creatief project schuilt een boek, schrift of journal waarin het onderwerp van alle kanten wordt onderzocht. Door een creative journal bij te houden, leg je de basis voor je eigen creatieve ontwikkeling.

Tijdens een creative journal sessie maak je een zine van 16 pagina’s. Je combineert oefeningen in tekenen, schrijven, ontwerpen en drukken. Een uniek document waarin je een persoonlijke herinnering op allerlei manieren hebt onderzocht en verbeeld.

Deelname aan de workshop is gratis. Wel willen we je vragen je aan te melden door een mail te sturen naar info@tacit.studio met als onderwerp workshop Creative Journal. Vergeet niet je naam, het aantal personen en het tijdstip van de workshop toe te voegen. Er is plek voor 6 deelnemers.

De workshop wordt gegeven door Roy Scholten. Scholten is naast service designer, informatie-architect en beeldend kunstenaar ook een van de partners van tacit.studio.

tacit.studio geeft cursussen en workshops aan media- en onderwijsprofessionals voor het trainen van je creatieve brein.

Locatie: Creative Image Lab in de Gooische Brink
Vrijdag 8 oktober
sessie 1: 10.00 - 12.30
sessie 2: 13.30 - 16.00
Aanmelden: info@tacit.studio
