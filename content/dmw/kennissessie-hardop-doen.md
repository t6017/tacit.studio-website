---
title: "Kennissessie Hardop Doen"
description: "Creatief met curriculum."
draft: false
cover: /images/promo-hardop-doen-grijs.jpg



---

### Hoe maak je als onderwijsprofessional vakoverstijging mogelijk binnen jouw onderwijspraktijk? Tijdens de kennissessie Hardop Doen krijg je tools om dit mogelijk te maken in jouw klas of bij jouw op school.

Een les ontwerpen vraagt iets anders van een docent dan lesgeven.  Bij het maken van lessen doe je een beroep op je creatieve vaardigheden zoals onderzoeken, analyseren, verbanden leggen en vooral doen! En misschien blijkt de oplossing dan wel dichterbij te liggen dan je denkt. Je zult zien dat er verbintenissen te maken zijn met andere onderwerpen en vakken die een interessant en nieuw licht op je eigen curriculum zullen doen schijnen. Aan de hand van een aantal concrete voorbeelden uit de praktijk ga je zelf aan de slag om nieuwe invalshoeken voor jouw lesprogramma te verzinnen.

### In de kennissessie Hardop Doen wordt je je (opnieuw) bewust van de stappen in een ontwerpproces.

Dit biedt je handvatten en geeft je de regie om je eigen onderwijs van een andere kant te bezien.

Deze kennissessie wordt begeleid door Esmee Hickendorff. Naast dat zij cultuurcoach is voor de gemeente Hilversum, kunsteducatie ontwikkelt voor jongeren en jonge professionals, projectcoördinator is voor Creatief Vermogen Utrecht (CMK3), is zij ook een van de partners van tacit.studio.

tacit.studio geeft cursussen en workshops aan media- en onderwijsprofessionals voor het trainen van je creatieve brein.  

Locatie: Creative Image Lab in de Gooische Brink  
Datum: Woensdag 6 oktober  
Tijdstip: 16.00 uur - 17.30 uur  
Aanmelden: info@tacit.studio
