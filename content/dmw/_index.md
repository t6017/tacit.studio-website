---
Title: tacit.studio @ dutch media week 2021
description: "Speciaal programma tijdens Dutch Media Week! Volg een van de gratis workshops of kom kennis maken in het Creative Image Lab."
draft: true
menu:
    sidebar:
        weight: 1
        name: DMW 2021
aliases:
    - /dmw/  
---

Speciaal programma tijdens Dutch Media Week! Volg een van de gratis workshops of kom kennis maken in het Creative Image Lab.
