---
title: "Workshop Print & Play"
description: "Spelen is wat ons mens maakt. Kom uit je hoofd en laat je handen spreken."
draft: false
cover: /images/promo-print-play-grijs.jpg

---

Met Print & Play maak je op een speelse manier kennis met verschillende experimentele druktechnieken. Je gaat beeld ontwerpen en drukken zonder te hoeven tekenen en je onderzoekt combinaties met gedrukte letters.

### In deze speelse, creatieve workshop ga je aan de slag met monoprint en letterpress

Je komt snel tot een serie verrassende resultaten. Aan het einde van de workshop heb je een visueel verslag van je eigen experimentele beeldonderzoek.

Deelname aan de workshop is gratis. Wel willen we je vragen je aan te melden door een mail te sturen naar info@tacit.studio met als onderwerp workshop Print & Play. Vergeet niet je naam, het aantal personen en het tijdstip van de workshop toe te voegen. Er is plek voor 6 deelnemers.

De workshop wordt gegeven door Martijn van der Blom. Blom is naast leermeester, vakman en beeldend kunstenaar de drijvende kracht achter het kunsteducatieprogramma en de werkplaats van Grafisch Atelier Hilversum. Ook is hij een van de partners van tacit.studio. tacit.studio geeft cursussen en workshops aan media- en onderwijsprofessionals voor het trainen van je creatieve brein.  


Locatie:
Creative Image Lab  
Gooische Brink 63-35  
Donderdag 7 oktober  
Sessie 1: 17.30 - 18.30  
Sessie 2: 19.00 - 20.00  
Aanmelden: info@tacit.studio
