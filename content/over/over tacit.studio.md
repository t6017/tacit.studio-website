---
title: "team tacit"
draft: false

cover: /images/team-tacit.jpg

menu:
    sidebar:
        weight: 600
        name: team tacit
---

**Esmee Hickendorff** – Kunsteducatie ontwikkelaar, organisator en verbinder met speciale aandacht voor het ontwikkelen van cultureel aanbod en infrastructuur met en voor jongeren en jonge professionals.

**Roy Scholten** – Service designer, informatiearchitect en beeldend kunstenaar. Beleidt organisaties bij hun digitale transformatie, is open source product manager, navigeert complexiteit middels visualisatie.


**Martijn van der Blom**  – Beeldend kunstenaar, leermeester en vakman. Drijvende kracht achter het kunsteducatieprogramma en de werkplaats van Grafisch Atelier Hilversum.
