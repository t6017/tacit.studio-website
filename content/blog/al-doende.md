---
title: "Al doende leert men (iets nieuws)"
date: 2019-05-01T15:56:35+01:00
draft: false
type: article
---

Al doende leert men. De gangbare interpretatie van deze oude spreuk is dat je door herhaling, oefenen, een op voorhand vastgesteld doel of resultaat kunt bereiken. Als je wil leren blind te typen zul je moeten oefenen om de posities van de letters in te slijten. Als je heel Holland Bakt wilt winnen zul je thuis moeten oefenen met hoe je precies een Utrechtse sprits maakt.

In deze interpretatie van "al doende leert men" zijn zowel het doel als de bijbehorende oefeningen bekend. Door met intentie de gedefinieerde oefeningen uit te voeren zul je het beoogde resultaat kunnen bereiken.

Maar wat als het einddoel niet vooraf bekend is? Wat als alleen het vertrekpunt gegeven is met daarbij de erkenning dat "het" beter kan maar we weten nog niet hoe?

Ook dan geldt: al doende leert men. Het doen is dan gericht op vinden en  ontdekken. Doen als manier om onbekend terrein te verkennen, in kaart te brengen en daardoor wel bekend en navigeerbaar te maken.

Deze vorm van doen vraagt om een bepaalde attitude en benadering:

- Achterwege laten van verwachtingen over specifieke uitkomsten
- De wil om met open vizier te experimenteren: van "ik wil dat het … wordt" naar "wat zou er gebeuren als…"
- Meer is beter: genereren van verschillende opties

## Bij twijfel: maak iets

![Letterpress print: when in doubt, make something](/when-in-doubt-make-something.jpg)

Gun jezelf de tijd en de ruimte om te maken zonder specifieke verwachtingen over wat er ontstaat. Definieer een vertrekpunt en de globale richting die je op gaat, maar laat open wat uiteindelijk de exacte bestemming zal zijn.
 
Om dan ook echt te leren van dit doen zul je al snel weer een beroep willen doen op taal. Vind je de woorden om te beschrijven wat je ziet? Kun je benoemen wat de ontwikkelingen zijn geweest? Herken je waar het potentieel zit voor een volgende, gerichter verkenning?

Om conclusies te trekken en volgende stappen te bepalen zul je bewust willen kunnen reflecteren op wat je gemaakt en wat je daarmee dus bereikt hebt.

## Hardop Doen

Kunst en ontwerp zijn disciplines met een lange traditie waarin veel kennis is opgebouwd over hoe van niets tot iets te komen. Over hoe een bestaande situatie te veranderen in een betere.

Het nieuwe komt niet tot stand door een magische mix van goddelijke inspiratie en talent. Het nieuwe ontstaat door bewust en expliciet de stappen van een ontwerpproces toe te passen.

> "Het moet anders en het moet beter, maar hoe?"

Hoe vaak is dat niet *precies* wat er aan de hand is bij de uitdagingen van vandaag de dag? De vanzelfsprekende antwoorden met uitgeschreven stappenplannen voor succes zijn hier immers niet meer van toepassing.

Complexe vraagstukken pak je aan door al doende te leren. Experimenteer door te *doen* en *leer* van de resultaten en de feedback.

---

Wil je ook leren hoe een ontwerponderzoek kan helpen bij de aanpakken van een complexe uitdaging? Schrijf je dan in voor de [Hardop Doen Bootcamp op zaterdag 15 juni](/services/hardop-doen-bootcamp/) of hoor hoe andere professionals deze aanpak al in de praktijk brengen tijdens de [infoavond over makerschap op 15 mei](/blog/infoavond-mei-2019/) aanstaande.

