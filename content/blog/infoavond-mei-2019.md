---
title: "15 mei infoavond Hardop Doen: makerschap en toegepaste creativiteit"
date: 2019-03-12T15:56:35+01:00
draft: true
type: article
---

Tijdens de tacit infoavonden vertellen ontwerpers, kunstenaars en educatiespecialisten over hun creatieve beroepspraktijk.

Donderdag 15 mei 2019 trappen we af met een avond waarin we makerschap centraal stellen.

## Programma

We starten "direct uit het werk" zodat je daarna nog wat van je avond over hebt.

- 17:00 - 17:30 Inloop met brood en soep  
- 17:30 - 18:00 Presentatie over makerschap en toegepaste creativiteit door Roy Scholten.
- 18:00 - 18:30 Demonstratie "ontwerponderzoek met monoprint" door Martijn van der Blom.
- 18.30 - 19.30 Aan de slag: een kleine eerste oefening in Hardop Doen begeleid door Esmee Hickendorff.
- 19:30 - 20:00 Reflectie: hoe heb je hier wat aan in je werk?
- 20:00 - 20:30 Napraten en einde.

## Praktisch

Adres:
tacit.studio  
2e verdieping, kamer 213  
Noorderweg 96b  
1221AB Hilversum

Je kunt parkeren voor de deur. Het is 7 minuten lopen vanaf de achteruitgang van station Hilversum. Er is een lift aanwezig.

## Laat weten dat je komt

De toegang is gratis maar laat wel even weten dat je komt:

<form method="post" action="https://tacitstudio.email-provider.nl/subscribe/post/index.php" accept-charset="utf-8">
<input type="hidden" name="next" value="http://tacit.studio/subscribe/bedankt/" />
<input type="hidden" name="a" value="nevbzna7ef" />
<input type="hidden" name="l" value="716qhwc0kp" />
<label for="id-qctjflkx95">E-mailadres&nbsp;*</label><br><input type="text" name="qctjflkx95" id="id-qctjflkx95"><br>
<label for="id-9WiifFh6Xs">Voornaam</label><br><input type="text" name="9WiifFh6Xs" id="id-9WiifFh6Xs"><br>
<label for="id-yKkcRWeN5H">Achternaam</label><br><input type="text" name="yKkcRWeN5H" id="id-yKkcRWeN5H"><br>
<input type="submit" value="Aanmelden infoavond 15 mei" />
</form>
