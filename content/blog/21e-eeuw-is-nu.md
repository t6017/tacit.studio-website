---
title: "Reminder: de 21e eeuw = nu"
date: 2019-03-12T15:56:35+01:00
draft: false
type: blog
---

Met al die goede plannen, modellen en adviezen over hoe het eigenlijk zou moeten zou je haast vergeten dat de 21e eeuw al in haar 19<sup>e</sup> jaar zit.

De 21<sup>e</sup> eeuw is *nu* mensen. Hoe oncomfortabel het ook is (je bent er nooit *helemaal* klaar voor), de handen moeten uit de mouwen. De gewenste verandering realiseer je niet alleen met een plan.

Aan de slag!