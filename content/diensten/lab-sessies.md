---
title: "labsessies"
description: "dit is de strapline"
draft: true
cover: /images/image3.png
aliases:
  - /labsessies/


---

### Al een tijd geen aandacht gehad voor je eigen creativiteit? Terwijl je juist nu wel toe bent aan tijd en aandacht voor jezelf. Maak weer contact met je eigen verbeelding tijdens onze lab sessies en zet de volgende stap in je creatieve ontwikkeling.

Midden in het centrum van Hilversum vind je het Creative Image Lab, de plek om op creatieve manier te maken, te onderzoeken en te ontwikkelen. De komende periode kun je er op de zondagmiddag terecht voor korte labsessies.

**[AANMELDEN LABSESSIE 22 AUGUSTUS](mailto:info@tacit.studio?subject=labsessie%2022%20Augustus)**

## Ervaar in anderhalf uur weer hoe het is om te *maken* zonder te *moeten*.

Je maakt kennis met de basis van "Print & Play" en "Creative Journal".

**Print & Play** richt zich op onderzoeken met beeld. Met een custom druktechniek start je spelenderwijs je eigen ontwerponderzoek en ervaar je wat een ontwerpproces inhoudt.

**Creative Journal** combineert schrijven en tekenen. Ervaar hoe je een verhaal uit jezelf kunt halen door te associëren in woord en beeld.

Aan het eind heb je een visueel verslag van je onderzoek: je verbeelding in beeld gebracht. Maar het belangrijkste resultaat is dat je ongestoord en ongedwongen met hoofd, hand en hart bezig bent geweest.

## Labsessie - 22 augustus 2021

- Max. 4 deelnemers per sessie
- Iedereen heeft een ruime, eigen werkplek
- van 13.30 tot 15:00 uur of van 15.30 - 17:00 uur
- Aanmelden is nodig: [stuur een mail](mailto:info@tacit.studio?subject=labsessie%2022%20Augustus)
- Kosten per persoon: €25,- (je kunt pinnen)

De lokatie is het [Creative Image Lab](/projecten/creative-image-lab/), in de passage Gooische Brink, Kerkbrink 63-35 in Hilversum.

**[AANMELDEN LABSESSIE 22 AUGUSTUS](mailto:info@tacit.studio?subject=labsessie%2022%20Augustus)**

---

### Volgende edities

- Labsessie – 19 september 2021
- Labsessie - 17 oktober 2021
- Labsessie - 21 november 2021
