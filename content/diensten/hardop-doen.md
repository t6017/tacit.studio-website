---
title: "Hardop Doen"
description: "De creatieve laadpaal voor docenten"
date: 2021-06-24T15:01:35+01:00
cover: /images/hardop-doen.png
weight: 3
draft: false
aliases:
  - /hardop-doen/

---

### Nét te weinig tijd, gebrek aan geschikte ruimte en een weinig flexibel curriculum. Hoe ontwikkel je als vakdocent beeldende vorming dan tóch vernieuwend cultuuronderwijs?

Investeer tijd in jezelf. Schep ruimte voor vernieuwing. Ga zélf weer maken. Vanuit jouw creatieve kracht kun jij jouw onderwijspraktijk ontwikkelen en verbeteren.

Praten met vakgenoten over uitdagingen, ervaringen delen en van elkaar´s expertise leren, is niet alleen prettig maar ook nuttig en belangrijk. In de praktijk is hier niet altijd tijd of ruimte voor.

Bij het coachingstraject Hardop Doen is deze ruimte er wel. Er is tijd om te ontmoeten en te verbinden.

## Een opfrisser voor je creatieve brein
Tijdens het traject ga je met vakgenoten in een kleine groep samen maken en onderzoeken. Zo (her)ontdek je jouw eigen makerschap en vergroot je jouw creatieve kracht. Door de combinatie van denken en doen, fris jij je creatieve brein op.

Je onderzoekt tegen welke uitdagingen je aanloopt. Je benadert dit van verschillende kanten door onderling vragen uit te wisselen en te reflecteren. Je stelt jezelf een onderzoeksvraag, bekijkt verschillende opties en sluit af met het selecteren van een oplossing.

Hierbij doorloop je in het kort de verschillende stappen van een creatief proces. Omdat je dit proces doorloopt met vakgenoten, zul je op inhoudelijk vlak ervaringen en frustraties maar ook ideeën en inspiratie uit de dagelijkse praktijk van het cultuuronderwijs met elkaar delen.

## Het coachingstraject brengt je het volgende:
Na deze coaching kun je met energie en inspiratie nieuwe stappen gaan zetten. Je kijkt op een andere manier naar bijvoorbeeld je eigen lesprogramma, de mogelijkheden voor vakoverstijging binnen jouw onderwijsinstelling of het ontwerpen van nieuwe lessen. Door verschillende tricks en tools te benoemen, te onderzoeken en uit te werken, zul je zien dat binnen de beperkende aspecten tijd, ruimte en methode er toch plek is voor nieuwe ideeën en inzichten. Door je eigen creatief makerschap weer te activeren kun je na 3 sessies geïnspireerd en opgeladen weer aan de slag op je eigen school.

## Programma
Het coachingstraject Hardop Doen bestaat uit 3 sessies met elk een eigen onderwerp:

**DOEN: activeer je creatief makerschap**  
Je gaat onder begeleiding van kunstenaar Martijn van der Blom aan de slag  met experimentele druktechnieken. Je komt uit je hoofd en laat je handen weer spreken. Verras jezelf met de resultaten en herontdek de lol van het zelf maken.

**DEEL: creatief met curriculum**  
Samen met kunsteducator- en ontwikkelaar Esmee Hickendorff reflecteer je  op het in sessie 1 gemaakte werk.Door het delen van kennis, ervaringen en voorbeelden uit de praktijk van het cultuuronderwijs, leer je van jouw vakgenoten en kom je tot nieuwe, creatieve inzichten.

**MAAK: creëer een (les)plan**  
Op basis van de input en lessen van de eerste 2 sessies maak je een eigen lesplan. Wat ga je doen? Welke uitgangspunten ga je hierbij hanteren? En welke “probe” ga je uitvoeren?  

## Praktisch
Locatie: Grafisch Atelier Hilversum  
Deelnemers: max. 5 per training  
3 sessies (startdatum nader te bepalen)  
Tarief: € 500,- exclusief BTW

**Heb je interesse of wil je meer weten over het coachingstraject? Of wil je met een groep collega’s van je school aan de slag en ben je op zoek naar een maatwerkoplossing? Neem dan contact op met info@tacit.studio.**
