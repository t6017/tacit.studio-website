---
title: "Visueel Manifest"
description: "Groepstraining: vind én verbeeld in 1 dag jullie collectieve ambitie"
date: 2019-02-14T12:56:35+01:00
cover: /images/visueel-manifest-header.jpg
draft: false
weight: 6
aliases:
    - /visueel-manifest/   
---

### Sta je aan het begin van een nieuwe samenwerking? Start je een groot nieuw project met nieuwe collega’s? Zit je al in een team maar zie je dat het samenwerken beter kan?

Niet alle groepen zijn ook echt teams. Effectief samenwerken vraagt om een solide basis van gedeelde waarden en uitgangspunten.

In de hands-on training Visueel Manifest definieer je die teamwaarden. Samen geef je deze waarden vorm in tekst en beeld.

> Visueel Manifest is een 1-daagse design sprint voor het visualiseren van je teamcultuur.

Tijdens de training doorloop je als groep de verschillende stappen van het ontwerpproces: uitdaging, opties, selectie, creatie, uitvoering en presentatie. De groep combineert denkwerk met fysiek maken.

![](/images/visueel-manifest-actie.jpg)

### Deze training brengt je als groep het volgende:
- Samen ontwerpen is samen onderzoeken, benoemen en beslissen. Zo leer je elkaar beter kennen.
- Maken is een vorm van denken. Je gaat aan het werk met je hoofd, je hart én je handen. Je ervaart de lol van het maken.
- Aan het eind van de training  zie je hoe ver je gekomen bent en wat daar bij kwam kijken om het te realiseren. Het proces wordt als vanzelf gedocumenteerd.
- Ontwerpen is een skill for life. Na de training weet je hoe je het ontwerpproces kunt toepassen op andere uitdagingen..
- Je verrast jezelf en je team met wat je blijkt te kunnen maken.

De inspirerende omgeving van een ambachtelijke drukkerswerkplaats biedt de rust en ruimte om het werk de aandacht te geven die het verdient.

### Voor wie

Voor professionals die verandering in gang willen brengen en daarbij de groep mee moet krijgen. Voor teams aan het begin van een nieuw traject. Voor groepen die elkaar beter willen leren kennen. Voor elke club mensen die nieuw terrein gaat betreden.

> Er is geen kaart, dus zet je kompas en vind samen de weg voorwaarts.

## Programma

Met Visueel Manifest werk je als groep aan een gezamenlijk kunstwerk. Een creatieve verbeelding van de kernwaarden waar je als groep voor staat.

Daarbij doorloop je als groep de stappen van een ontwerpproces:

- **Uitdaging** - Wat is de vraag waarop we een antwoord gaan geven?
- **Genereren** - Het genereren van zoveel mogelijk opties en ideeën.
- **Selecteren** - Wat zijn de beste ideëen? Welke gaan we ook echt uitwerken?
- **Creëren** - De geselecteerde ontwerpideeën worden verder uitgewerkt. Welke tekst- en beeld-combinatie werkt het best?
- **Uitvoeren** -  Druk je ontwerp af op de traditionele persen in de drukkerswerkplaats.
- **Presenteren** - We sluiten af met een korte reflectie op het proces en de resultaten.

## Praktisch

Lokatie: Grafisch Atelier Hilversum  
Deelnemers: min 6, max. 16 per training

**Tarief:**  
Halve dag vanaf €1.350,-  
Hele dag vanaf €2.250,-
Bedragen zijn exclusief 21% BTW.

**Heb je interesse of wil je meer weten over hoe deze training het best in te zetten voor jouw team? Neem dan contact op met info@tacit.studio.**
