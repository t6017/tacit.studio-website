---
title: "Creative Journal"
description: "Ontwikkel je ideëen en je makerschap"
draft: false
weight: 2
cover: /images/creative-journal.jpg
aliases:
    - /creative-journal/  

---

## Hoe ontwerp je een idee?

Creativiteit, verbeelding, ‘design thinking’, we worden allemaal geacht het te kennen en toe te passen. Maar wat houdt het in om creatief te zijn? Waar komt een idee vandaan en hoe “vang” je het?

Hét onmisbare instrument voor elke creatieve maker en visionaire veranderaar is haar schets- en notitieboek. Want achter elk creatief project schuilt een boek, schrift of journal waarin het onderwerp van alle kanten wordt onderzocht. Door zelf ook een creative journal bij te houden leg je de basis voor je eigen creatieve ontwikkeling.

## Inhoud

Met verschillende combinaties van tekenen en schrijven breng je in kaart wat je bezighoudt. Je traint zo je creatieve brein, in de veilige omgeving van je eigen journal.

> Een creative journal bijhouden is een vorm van zelf-onderhoud

- Begin te maken zonder oordeel - creëer een veilige plek voor je eigen experimenten
- Train je opmerkingsvermogen: kijken en zien, luisteren en horen
- Ontdek wat je inspireert en waarom: pas een ontwerpproces toe op je eigen interesses
- Maak kennis met de bouwstenen van visueel ontwerp: beeldelementen en compositie
- Leer de verschillende soorten tekening die je kunt maken
- Gebruik diverse werkvormen om ideeen te bedenken
- Creëer zo al doende een rijke voedingsbodem voor je eigen projecten

In elk van de vier sessies komt een combinatie van techniek, theorie en concept aan bod.

- Je oefent verschillende technieken om ideeën te genereren en in tekst en beeld om te zetten.
- Korte theoretische onderdelen zorgen voor verdieping en inspirerende voorbeelden.
- Concept staat voor reflecteren op wat je gemaakt hebt: wat spreekt je aan en waarom? Hoe kun je dat verder brengen?

## Praktisch
Tijden: donderdagavond van 18:30 tot 21:00 uur  
Data: 3, 10, 17 & 24 februari 2022  
Lokatie: Creative Image Lab in de Gooische Brink, Kerkstraat 63-35, Hilversum  
Minimaal vier, maximaal zes deelnemers  
Introductieprijs: €275,- (reguliere prijs: €350,-) exclusief BTW.  
Materialen: je hoeft geen materialen aan te schaffen.  

## Voor wie
Creative Journal is voor professionele makers. Ontwerpers, vormgevers, programma- en media- en andere makers die hun creatieve praktijk willen verdiepen.

Creative Journal wordt gegeven door [Roy Scholten](/over/over-tacit.studio/) – service designer, informatiearchitect en beeldend kunstenaar. Hij begeleidt organisaties bij hun digitale transformatie, is open source product manager en navigeert complexiteit middels visualisatie.

**Meedoen? Wil je weten of deze training bij je past? Mail naar info@tacit.studio.**
