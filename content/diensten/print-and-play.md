---
title: "Print & Play"
description: "Vrij beeldexperiment met grafische technieken"
draft: false
weight: 1
cover: /images/promo-print-play-grijs.jpg
aliases:
    - /print-play/        
---
### Kom uit je hoofd en laat je handen spreken 

Met Print & Play maak je in drie sessies kennis met een aantal grafische technieken die je stimuleren het vrije beeldexperiment aan te gaan.

Niet op het scherm maar op papier.  
Zonder computers maar met drukpersen.  
Geen actiepunten maar vrij spel.

## Programma

We beginnen met monoprint. Monoprint is een snelle en speelse manier van beeldmaken. Je komt al snel tot verrassende series werken.

In de tweede sessie maak je kennis met letterpress. Je gebruikt houten drukletters om een tekst of typografisch beeld in elkaar te zetten en af te drukken.

De derde sessie gebruik je om de technieken te combineren. Je gaat door op wat je de vorige keren gemaakt hebt of je maakt juist nog een heel nieuwe serie werken.

## Data, tijden, prijs

Dinsdagavond van 19:30 tot 22:00 uur  
Lokatie: Grafisch Atelier Hilversum  
Noorderweg 96b 1221AB Hilversum  

Startdatum in overleg. Als dat beter uitkomt hoef je niet drie keer achter elkaar te komen. Je werkt in een kleine groep: tot vijf personen per keer.

- Prijs: €150,- inclusief materialen, koffie, thee en BTW.
- Instructeur: Roy Scholten
- Meer info en aanmelden via info@tacit.studio
